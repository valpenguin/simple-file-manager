<?php
/**
 * PHP File Manager (2018-06-11)
 * Valerio Guaglianone (aka Valpenguin)
 * Config file
 */
 
// Auth with login/password (set true/false to enable/disable it)
$use_auth = false;
// Users: array('Username' => 'Password', 'Username2' => 'Password2', ...)
$auth_users = array(
    'admin' => 'admin',
	);
// Add hide/exclude folders
$restricted_folders = array(
        'hide','.cache',
);
// Enable highlight.js (https://highlightjs.org/) on view's page
$use_highlightjs = true;
// highlight.js style
$highlightjs_style = 'docco';
// See other CSS themes at: https://highlightjs.org/static/demo/

// Default timezone for date() and time() - http://php.net/manual/en/timezones.php
$default_timezone = 'Europe/Rome'; // UTC+2
// Root path for file manager
$root_path = $_SERVER['DOCUMENT_ROOT'] . "/test_web/";
// Root url for links in file manager.Relative to $http_host. Variants: '', 'path/to/subfolder'
// Will not working if $root_path will be outside of server document root
$root_url = '/test_web/';
// Server hostname. You can set manually if wrong
$http_host = $_SERVER['HTTP_HOST'];
// input encoding for iconv
$iconv_input_encoding = 'CP1251';
// date() format for file modification date
$datetime_format = 'd/m/Y H:i';
?>